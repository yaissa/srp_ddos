import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from skmultiflow.data import FileStream
from skmultiflow.meta import StreamingRandomPatchesClassifier
from skmultiflow.trees import ExtremelyFastDecisionTreeClassifier
from skmultiflow.evaluation import EvaluatePrequential

def test_run():
    start_time = time.time()

    stream = FileStream('bot-iot.csv')

    efdt = ExtremelyFastDecisionTreeClassifier(tie_threshold=0.5, grace_period=100)
    srp = StreamingRandomPatchesClassifier(random_state=1,
                                             n_estimators=10, 
                                             base_estimator=efdt)
    


    # 3. Setup the evaluator
    evaluator = EvaluatePrequential(pretrain_size=100,
                                    max_samples=1000,
                                    output_file='results.csv', metrics=['accuracy', 'f1', 'recall', 'precision'])
    # 4. Run evaluation
    evaluator.evaluate(stream=stream, model=srp)

if __name__ == "__main__":
    test_run()

